# lua-zpool

Bindings to ZFS's zpool.

## Usage

```
zpool = require "lua-zpool"

pools = zpool.list()
for pool,pool_data in pairs(pools) do
	print("Pool name: ", pool_data["name"])
end
```

