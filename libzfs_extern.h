/*
 * Basic types
 * cddl/contrib/opensolaris/lib/libzfs/common/libzfs.h
 */
typedef struct zpool_handle zpool_handle_t;
typedef struct libzfs_handle libzfs_handle_t;
typedef enum pool_state {
   POOL_STATE_ACTIVE = 0,     /* In active use     */
   POOL_STATE_EXPORTED,    /* Explicitly exported     */
   POOL_STATE_DESTROYED,      /* Explicitly destroyed    */
   POOL_STATE_SPARE,    /* Reserved for hot spare use */
   POOL_STATE_UNINITIALIZED,  /* Internal spa_t state    */
   POOL_STATE_UNAVAIL,     /* Internal libzfs state   */
   POOL_STATE_POTENTIALLY_ACTIVE /* Internal libzfs state   */
} pool_state_t;

/*
 * libzfs exported functions
 * cddl/contrib/opensolaris/lib/libzfs/common/libzfs.h
 */
typedef int (*zpool_iter_f)(zpool_handle_t *, void *);
extern int zpool_iter(libzfs_handle_t *, zpool_iter_f, void *);
extern const char *zpool_get_name(zpool_handle_t *);
extern int zpool_get_state(zpool_handle_t *);
extern const char *zpool_pool_state_to_name(pool_state_t);

/*
 * Library initialization
 * cddl/contrib/opensolaris/lib/libzfs/common/libzfs.h
 */
extern libzfs_handle_t *libzfs_init(void);
extern void libzfs_fini(libzfs_handle_t *);
