zpool = require "lua-zpool"

pools = zpool.list()

for pool,pool_data in pairs(pools) do
	print("Pool ID: ", pool)
	print(" Pool name: ", pool_data["name"])
	for k,v in pairs(pool_data) do
		print("\t", k,v)
	end
end
