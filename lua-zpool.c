#include "libzfs_extern.h"
#include <stdlib.h>

#include <lua.h>
#include <lauxlib.h>

// Temp include for printf
#include <stdio.h>

#define luaL_newlibtable(L, l) \
	(lua_createtable(L, 0, sizeof(l)/sizeof(*(l))-1))
#define luaL_newlib(L, l) \
	(luaL_newlibtable(L, l), luaL_register(L, NULL, l))

libzfs_handle_t *g_zfs;


static int
libzfs_open(void)
{
	if ((g_zfs = libzfs_init()) == NULL) {
		printf("internal error: failed to initialize ZFS library\n");
		return 0;
	}

	return 1;
}

static void
libzfs_close(void)
{
	libzfs_fini(g_zfs);
}

static int
zpool_list_callback(zpool_handle_t *zhp, void *data)
{
	lua_State *L = data;
	// Create a table to hold the zpool
	lua_newtable(L);

	lua_pushstring(L, zpool_get_name(zhp));
	lua_setfield(L, -2, "name");
	lua_pushnumber(L, zpool_get_state(zhp));
	lua_setfield(L, -2, "state");

	// Push our table into the outer group
	lua_setfield(L, -2, zpool_get_name(zhp));
	// zpool_iter() needs a 0 for success
	return 0;
}

static int
zpool_list(lua_State *L)
{
	// Create a new table to group the zpools
	lua_newtable(L);
	// Iterate over all pools in the system
	zpool_iter(g_zfs, zpool_list_callback, L);
	// Our table is the last value on the stack
	return 1;
}

static const luaL_Reg lib[] = {
	{"list",		zpool_list},
	{NULL,		NULL}
};

/*
 * Main entry point
 * Lua calls luaopen_modulename on a require
 */
int
luaopen_zpool(lua_State *L)
{
	// Register our functions with lua
	luaL_newlib(L, lib);
	// Initialize libzfs
	libzfs_open();
	// Register our closing function with the system
	atexit(libzfs_close);

	return 1;
}
