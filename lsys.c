#include <sys/cdefs.h>
#include <sys/param.h>
#include <err.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <lua.h>
#include <lauxlib.h>

#define luaL_newlib( L, l ) ( lua_newtable( L ), luaL_register( L, NULL, l ) )

static int l_gethostname(lua_State *L) {
	char hostname[MAXHOSTNAMELEN];

	gethostname(hostname, (int)sizeof(hostname));
	lua_pushlstring(L, hostname, (int)sizeof(hostname));

	return 1;
}

static const struct luaL_Reg lib_list[] = {
	{"l_gethostname", l_gethostname},
	{NULL, NULL}
};

int luaopen_lsys(lua_State *L) {
	luaL_newlib(L, lib_list);

	return 1;
}
